console.log("Sidenav script loaded");

const sidenav = document.querySelector('.sidenav');
const container = document.querySelector('.pageContainer');

console.log(sidenav);
console.log(container);

 /* Open the sidenav */
function openNav() {
  sidenav.style.display = "grid";
  container.style.backgroundColor = "#D9D7CC";
};

/* Close/hide the sidenav */
function closeNav() {
  sidenav.style.display = "none";
  container.style.backgroundColor = "#FFFDF0";
};